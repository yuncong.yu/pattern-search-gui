import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PlotlyModule } from 'angular-plotly.js';
import * as PlotlyJS from 'plotly.js/dist/plotly.js';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConfidenceViewComponent } from './confidence-view/confidence-view.component';
import { MainViewComponent } from './main-view/main-view.component';
import { QueryViewComponent } from './query-view/query-view.component';
import { StateService } from './state.service';
// import { TmpComponent } from './tmp/tmp.component';

PlotlyModule.plotlyjs = PlotlyJS;

@NgModule({
  declarations: [
    AppComponent,
    QueryViewComponent,
    MainViewComponent,
    // TmpComponent,
    ConfidenceViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    PlotlyModule
  ],
  providers: [
    StateService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
