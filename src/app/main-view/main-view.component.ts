import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { StateService } from '../state.service';

declare var Plotly: any;

@Component({
  selector: 'app-main-view',
  templateUrl: './main-view.component.html',
  styleUrls: ['./main-view.component.css']
})
export class MainViewComponent implements OnInit {

  dataHeatmap = [];
  layoutHeatmap = {};

  dataLinePlot = [];
  subplots: string[][] = [];
  annotationLinePlot = [];
  layoutLinePlot: {};

  config: {}
  localXAxisRange: number[] = null;

  @ViewChild("mainHeatmap")
  graphHeatmap: ElementRef;
  @ViewChild("mainLinePlot")
  graphLinePlot: ElementRef;

  constructor(private state: StateService) { 
    this.prepareTimeSeriesTrace();
    this.prepareTimeSeriesReconstructedTrace();
    this.prepareLabelsTraces();
    this.preparePredictionsTraces();
    this.prepareLayoutConfig();
  }

  ngOnInit(): void {
    this.graphHeatmap = Plotly.react('mainHeatmap', this.dataHeatmap, this.layoutHeatmap, this.config);
    this.graphLinePlot = Plotly.react('mainLinePlot', this.dataLinePlot, this.layoutLinePlot, this.config);
  }

  ngAfterViewInit() {
    // (Oberservable based solution)
    // Publish x-range change 
    // this.graph.nativeElement.on(
    //   "plotly_relayout",
    //   // Unfortunately, this handler can't be put in a seperate function, 
    //   // otherwise, "this" would be the event itself but not the component class
    //   (data: {}) => {
    //     // Avoid recursive resizing
    //     if ("xaxis.range" in data) return
    //     // if ("xaxis.autorange" in data && this.localXAxisRange !== null) {
    //     if ("xaxis.autorange" in data) {
    //       this.localXAxisRange = null;
    //       this.state.mainViewXAxisRange = this.localXAxisRange;
    //     }
    //     if ("xaxis.range[0]" in data) {
    //       this.localXAxisRange = [data['xaxis.range[0]'], data['xaxis.range[1]']];
    //       this.state.mainViewXAxisRange = this.localXAxisRange;
    //     }
    //   }
    // )

    this.graphHeatmap.nativeElement.on(
      "plotly_relayout",
      // Unfortunately, this handler can't be put in a seperate function, otherwise, "this" would be the event itself but not the component class
      (data: {}) => {
        if ("xaxis.autorange" in data && this.state.mainViewXAxisRange !== null) {
          this.state.mainViewXAxisRange = null;
          Plotly.relayout("mainLinePlot", { 'xaxis.autorange': true });
          Plotly.relayout("confidenceView", { 'xaxis.autorange': true });
        }
        if ("xaxis.range[0]" in data ) {
          this.state.mainViewXAxisRange = [data['xaxis.range[0]'], data['xaxis.range[1]']];
          Plotly.relayout("mainLinePlot", { 'xaxis.range': this.state.mainViewXAxisRange })
          Plotly.relayout("confidenceView", { 'xaxis.range': this.state.mainViewXAxisRange })
        }
        if ("xaxis.range" in data) {
          this.state.mainViewXAxisRange = [data["xaxis.range"][0], data["xaxis.range"][1]];
          Plotly.relayout("mainLinePlot", { 'xaxis.range': this.state.mainViewXAxisRange })
          Plotly.relayout("confidenceView", { 'xaxis.range': this.state.mainViewXAxisRange })
        }  
      }
    )

    this.graphLinePlot.nativeElement.on(
      "plotly_relayout",
      // Unfortunately, this handler can't be put in a seperate function, otherwise, "this" would be the event itself but not the component class
      (data: {}) => {
        // console.log(data)
        // if ("xaxis.autorange" in data) {
        if ("xaxis.autorange" in data && this.state.mainViewXAxisRange !== null) {
          this.state.mainViewXAxisRange = null;
          Plotly.relayout("mainHeatmap", { 'xaxis.autorange': true });
          Plotly.relayout("confidenceView", { 'xaxis.autorange': true });
        }
        if ("xaxis.range[0]" in data ) {
          this.state.mainViewXAxisRange = [data['xaxis.range[0]'], data['xaxis.range[1]']];
          Plotly.relayout("mainHeatmap", { 'xaxis.range': this.state.mainViewXAxisRange })
          Plotly.relayout("confidenceView", { 'xaxis.range': this.state.mainViewXAxisRange })
        }
      }
    )
    
    // (Oberservable based solution)
    // Subscribe to x-range change
    // this.state.mainViewXAxisRangeObservable.subscribe(this.updateLocalXAxis)
  }

  prepareTimeSeriesTrace() {
    // Heatmap
    this.dataHeatmap.push({
      z: this.state.timeSeries.slice(1).map(
        (vChannel: number[]) => {
          let zMinChannel = Math.min(...vChannel);
          let zMaxChannel = Math.max(...vChannel);
          let scalingFactor = 1 / (zMaxChannel - zMinChannel);
          return vChannel.map(v => (v - zMinChannel) * scalingFactor)
        }
      ),
      x: this.state.timeSeries[0],
      type: "heatmap",
      xaxis: "x",
      yaxis: "y",
      colorscale: 'YlGnBu',
      showscale: false,
      colorbar: {
        showticklabels: false
      }
    })

    // Line plot
    this.state.timeSeries.slice(1).forEach(
      (valuesChannel: number[], index: number) => {

        // Construct and add a trace of the origianl time series
        this.dataLinePlot.push({
          x: this.state.timeSeries[0],
          y: valuesChannel,
          mode: "lines",
          xaxis: "x",
          yaxis: `y${index + 2}`,
          line: { color: '#1f77b4' },  // muted blue (tableau blue)
          name: "original",
          legendgroup: "original",
          showlegend: !index,
        })

        // Add a subplot for the trace
        this.subplots.push([`xy${index + 2}`])

        // Add subplot titles
        this.annotationLinePlot.push({
          xref: "x domain",
          yref: `y${index+2} domain`,
          xanchor: "left",
          yanchor: "bottom",
          x: 0.5,
          y: 1,
          text: `<b>Channel ${index+1}</b>`,
          showarrow: false
        })
      }
    );
  }

  prepareTimeSeriesReconstructedTrace() {
    this.state.timeSeriesReconstructed.slice(1).forEach(
      (valuesChannel: number[], index: number) => {

        // Construct and add a trace of the reconstructed time series
        this.dataLinePlot.push({
          x: this.state.timeSeriesReconstructed[0],
          y: valuesChannel,
          mode: "lines",
          xaxis: "x",
          yaxis: `y${index + 2}`,
          line: { 
            color: '#ff7f0e',  // safety orange
            dash: "dot" 
          },
          name: "reconstructed",
          legendgroup: "reconstructed",
          showlegend: !index,
        })

        // Add a subplot for the trace
        this.subplots.push([`xy${index + 2}`])
      }
    )
    
    // Add fictive traces in case the reconstructed data have less channels than the original data
    for (let i = this.state.timeSeriesReconstructed.length - 1; i < this.state.timeSeries.length - 1; i++) {
      this.dataLinePlot.push({
        x: null,
        y: null,
        mode: "lines",
        xaxis: "x",
        yaxis: `y${i + 2}`,
        line: { 
          color: '#ff7f0e',  // safety orange
          dash: "dot" 
        },
        name: "reconstructed",
        legendgroup: "reconstructed",
        showlegend: false,
      })
      this.subplots.push([`xy${i + 2}`])
    }
  }

  prepareLabelsTraces() {
    let positions = this.state.labels.map(label => (label[0] + label[1]) / 2);
    let width = this.state.labels.map(label => (label[1] - label[0]));

    this.state.timeSeries.slice(1).forEach(
      (valuesChannel: number[], indexChannel: number) => {

        let yMin = Math.min(...valuesChannel);
        let yMax = Math.max(...valuesChannel);
        let yRange = yMax - yMin;

        const bottomTrace = {
          x: positions,
          y: Array(this.state.labels.length).fill(yMin),
          xaxis: "x",
          yaxis: "y" + (indexChannel + 2),
          width: width,
          type: "bar",
          marker: {
            color: "rgba(1, 1, 1, 0.0)",
            line: { width: 0 }
          },
          legendgroup: "labels",
          showlegend: false
        };

        const topTrace = {
          x: positions,
          y: Array(this.state.labels.length).fill(yRange),
          xaxis: "x",
          yaxis: "y" + (indexChannel + 2),
          width: width,
          type: "bar",
          marker: {
            color: '#17becf',  // blue-teal
            line: { 
              // color: '#17becf',
              width: 0 
            }
          },
          opacity: 0.25,
          name: "labels",
          legendgroup: "labels",
          showlegend: !indexChannel
        };

        this.dataLinePlot.push(bottomTrace);
        this.dataLinePlot.push(topTrace);
      }
    )
  }

  preparePredictionsTraces() {
    let chosenPreds = this.state.getPredWithConfAboveThres();
    let positions = chosenPreds.map(pred => (pred[0] + pred[1]) / 2);
    let width = chosenPreds.map(pred => (pred[1] - pred[0]));
    let text = chosenPreds.map(pred => pred[2].toFixed(2))

    this.state.timeSeries.slice(1).forEach(
      (valuesChannel: number[], indexChannel: number) => {

        let yMin = Math.min(...valuesChannel);
        let yMax = Math.max(...valuesChannel);
        let yRange = yMax - yMin;

        const bottomTrace = {
          x: positions,
          y: Array(chosenPreds.length).fill(yMin),
          xaxis: "x",
          yaxis: "y" + (indexChannel + 2),
          width: width,
          type: "bar",
          marker: {
            color: "rgba(1, 1, 1, 0.0)",
            line: { width: 0 }
          },
          legendgroup: "predictions with confidence",
          showlegend: false
        };

        const topTrace = {
          x: positions,
          y: Array(chosenPreds.length).fill(yRange),
          xaxis: "x",
          yaxis: "y" + (indexChannel + 2),
          width: width,
          text: text,
          type: "bar",
          marker: {
            color: '#2ca02c',  // cooked asparagus green (tableau green)
            line: { width: 0 }
          },
          opacity: 0.3,
          name: "predictions with confidence",
          textposition: 'auto',
          hoverinfo: 'none',
          legendgroup: "predictions",
          showlegend: !indexChannel
        };

        this.dataLinePlot.push(bottomTrace);
        this.dataLinePlot.push(topTrace);
      }
    )
  }

  prepareLayoutConfig(): void {
    // Heatmap
    this.layoutHeatmap = {
      // title: "Color Encoded Query",
      xaxis: {
        rangeslider: {}
      },
      yaxis: {
        autorange: "reversed",
        visible: false
      },
      height: 50 * this.state.query.length - 1,
      margin: {
        l: 30,
        r: 30,
        t: 30,
        b: 30,
        pad: 4
      },
    }

    // Line Plot
    this.layoutLinePlot = {
      // xaxis: (this.state.xaxisRange == null) ? {"autorange": true} : {"range": this.state.xaxisRange}, 
      // Range slider
      // xaxis: { 
      //   rangeslider: {
      //     visible: true
      //   },
      //   autorange: true,
      // }, 
      // xaxis2: {
      //   matches: 'x', //Important for slider to work properly for all traces
      //   overlaying: 'x' //Important for hover to work properly for all traces
      // },
      // yaxis: { 
      //   fixedrange: false
      // },
      grid: { rows: this.state.timeSeries.length - 1, columns: 1, subplots: this.subplots },
      barmode: "stack",
      height: 150 * this.state.timeSeries.length - 1,
      legend: {
        "orientation": "h",
        "traceorder": "normal",
        // x: 0,
        // y: 1.05
      },
      margin: {
        l: 30,
        r: 30,
        t: 30,
        b: 30,
        pad: 4
      },
      annotations: this.annotationLinePlot
    }

    this.config = { 
      responsive: true,
      displaylogo: false
    };
  }

  // (Oberservable based solution)
  // updateLocalXAxis(newRange: number[]) {
  //   if (newRange == null && this.localXAxisRange !== null) {
  //     this.localXAxisRange = null;
  //     Plotly.relayout("mainView", { 'xaxis.autorange': true })
  //   }
  //   // else if (JSON.stringify(newRange) != JSON.stringify(this.localXAxisRange)) {
  //   else {
  //     this.localXAxisRange = newRange;
  //     Plotly.relayout("mainView", { 'xaxis.range': newRange })
  //   }
  // }
}
