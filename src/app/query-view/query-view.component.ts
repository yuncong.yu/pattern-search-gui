import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { StateService } from '../state.service';

declare var Plotly: any;

@Component({
  selector: 'app-query-view',
  templateUrl: './query-view.component.html',
  styleUrls: ['./query-view.component.css']
})
export class QueryViewComponent implements OnInit {

  dataHeatmap = [];
  layoutHeatmap = {};

  dataLinePlot = [];
  subplotsLinePlot = [];
  annotationLinePlot = [];
  layoutLinePlot: {};

  config: {};

  // xAxisRange: BehaviorSubject<number[]> = new BehaviorSubject<number[]>(null);
  // xAxisRangeObervable: Observable<number[]> = this.xAxisRange.asObservable()
  xAxisRange = null;

  @ViewChild("queryHeatmap")
  graphHeatmap: ElementRef;
  @ViewChild("queryLinePlot")
  graphLinePlot: ElementRef;
  
  constructor(private state: StateService) { 
  }

  ngOnInit(): void {
    // Data - heatmap
    this.dataHeatmap.push({
      z: this.state.query.slice(1).map(
        (vChannel: number[]) => {
          let zMinChannel = Math.min(...vChannel);
          let zMaxChannel = Math.max(...vChannel);
          let scalingFactor = 1 / (zMaxChannel - zMinChannel);
          return vChannel.map(v => (v - zMinChannel) * scalingFactor)
        }
      ),
      x: this.state.query[0],
      type: "heatmap",
      xaxis: "x",
      yaxis: "y",
      colorscale: 'YlGnBu',
      showscale: false,
      colorbar: {
        showticklabels: false
      }
    })

    // Data - original - line plot
    this.state.query.slice(1).forEach(
      (v_channel: number[], index: number) => {        
        this.dataLinePlot.push({
          x: this.state.query[0],
          y: v_channel,
          type: "line",
          xaxis: "x",
          yaxis: `y${index + 2}`,
          marker: {color: '#1f77b4' },  // muted blue
          name: "original",
          legendgroup: "original",
          showlegend: !index,
        })
        this.subplotsLinePlot.push([`xy${index + 2}`])
        this.annotationLinePlot.push({
          xref: "x domain",
          yref: `y${index+2} domain`,
          xanchor: "center",
          yanchor: "bottom",
          x: 0.5,
          y: 1,
          text: `<b>Channel ${index+1}</b>`,
          showarrow: false
        })
      }
    );
    
    // Data - reconstructed - line plot
    this.state.query_reconstructed.slice(1).forEach(
      (v_channel: number[], index: number) => {
        this.dataLinePlot.push({
          x: this.state.query_reconstructed[0],
          y: v_channel,
          type: "line",
          xaxis: "x",
          yaxis: `y${index + 2}`,
          marker: {color: '#ff7f0e' }, // safety orange
          line: {dash: "dot"},
          name: "reconstructed",
          legendgroup: "reconstructed",
          showlegend: !index,
        })
        this.subplotsLinePlot.push([`xy${index + 2}`])
      }
    )

    // Layout - heatmap
    this.layoutHeatmap = {
      // title: "Color Encoded Query",
      xaxis: {
        rangeslider: {}
      },
      yaxis: {
        autorange: "reversed",
        visible: false
      },
      height: 50 * this.state.query.length - 1,
      margin: {
        l: 30,
        r: 30,
        t: 30,
        b: 30,
        pad: 4
      },
    }

    // Layout - line plot
    this.layoutLinePlot = {
      grid: { rows: this.state.query.length - 1, columns: 1, subplots: this.subplotsLinePlot },
      height: 150 * this.state.query.length - 1,
      // showlegend: false,
      // xaxis: {
      //   title: "time"
      // },
      legend: {
        "orientation": "h",
        // x: 0,
        // y: -0.05
      },
      margin: {
        l: 30,
        r: 30,
        t: 30,
        b: 30,
        pad: 4
      },
      annotations: this.annotationLinePlot
    }

    // Config
    this.config = { 
      responsive: true,
      displaylogo: false
    };

    // Plot
    this.graphHeatmap = Plotly.react("queryHeatmap", this.dataHeatmap, this.layoutHeatmap, this.config); 
    this.graphLinePlot = Plotly.react("queryLineplot", this.dataLinePlot, this.layoutLinePlot, this.config); 
  }

  // Bind the x-axis of the two plots
  ngAfterViewInit() {
    // Heatmap -> line plot
    this.graphHeatmap.nativeElement.on("plotly_relayout", (data) => {
      // if ("xaxis.range" in data) return
      if ("xaxis.autorange" in data && this.xAxisRange !== null) {
        this.xAxisRange = null;
        Plotly.relayout("queryLineplot", { 'xaxis.autorange': true })
      }
      if ("xaxis.range[0]" in data) {
        this.xAxisRange = [data['xaxis.range[0]'], data['xaxis.range[1]']];
        Plotly.relayout("queryLineplot", { 'xaxis.range': this.xAxisRange })
      } 
      if ("xaxis.range" in data) {
        this.xAxisRange = [data["xaxis.range"][0], data["xaxis.range"][1]];
        Plotly.relayout("queryLineplot", { 'xaxis.range': this.xAxisRange })
      }
    })
    
    // Line plot -> heatmap
    this.graphLinePlot.nativeElement.on("plotly_relayout", (data) => {
      // if ("xaxis.range" in data) return
      if ("xaxis.autorange" in data && this.xAxisRange !== null) {
        this.xAxisRange = null;
        Plotly.relayout("queryHeatmap", { 'xaxis.autorange': true })}
      if ("xaxis.range[0]" in data) {
        this.xAxisRange = [data['xaxis.range[0]'], data['xaxis.range[1]']];
        Plotly.relayout("queryHeatmap", { 'xaxis.range': this.xAxisRange })}

      
      // if ("xaxis.range" in data && (data["xaxis.range"][0] !== this.xAxisRange[0] || data["xaxis.range"][1] !== this.xAxisRange[1]) ) {
      //   this.xAxisRange = [data["xaxis.range"][0], data["xaxis.range"][1]];
      //   Plotly.relayout("queryHeatmap", { 'xaxis.range': this.xAxisRange })
      // }
    })
  }
}
