import { Injectable } from '@angular/core';
import queryFile from '../assets/deepValve/query.json';
import queryReconstructedFile from '../assets/deepValve/query_reconstructed.json';
import timeSeriesFile from '../assets/deepValve/time_series.json';
import timeSeriesReconstructedFile from '../assets/deepValve/time_series_reconstructed.json';
import labelsFile from '../assets/deepValve/labels.json'
import predictionsWithConfidenceFile from '../assets/deepValve/predictions_with_confidence.json'
import confidenceFile from '../assets/deepValve/confidence.json'
// import queryFile from '../assets/lastsprung/query.json';
// import queryReconstructedFile from '../assets/lastsprung/query_reconstructed.json';
// import timeSeriesFile from '../assets/lastsprung/time_series.json';
// import timeSeriesReconstructedFile from '../assets/lastsprung/time_series_reconstructed.json';
// import labelsFile from '../assets/lastsprung/labels.json'
// import predictionsWithConfidenceFile from '../assets/lastsprung/predictions_with_confidence.json'
// import confidenceFile from '../assets/lastsprung/confidence.json'

export interface SequentialDataJson {
  time: number[];
}

export interface SequentialData {
  channelNames: string[];
  data: number[][]
}

export interface LabelsJson {
  time_start: number,
  time_end: number,
  file: string
}[]

@Injectable({
  providedIn: 'root'
})
export class StateService {

  private _query: number[][];
  private _queryReconstructed: number[][];
  private _timeSeries: number[][];
  private _timeSeriesReconstructed: number[][];
  private _labels: number[][];
  private _predictionsWithConfidence: number[][];
  private _threshold: number = 0.8;
  private _confidence: number[][];
  private _mainViewXAxisRange: number[] = null;
  // private _mainViewXAxisRange: BehaviorSubject<number[]>  = new BehaviorSubject<number[]>(null);
  // mainViewXAxisRangeObservable: Observable<number[]> = this._mainViewXAxisRange.asObservable();

  constructor() {
    this._query = this.readSequentialData(queryFile).data;
    this._queryReconstructed = this.readSequentialData(queryReconstructedFile).data;
    this._timeSeries = this.readSequentialData(timeSeriesFile).data;
    this._timeSeriesReconstructed = this.readSequentialData(timeSeriesReconstructedFile).data;
    this.readLabels();
    this.readPredictionsWithConfidence();
    this._confidence = this.readSequentialData(confidenceFile).data;
  }

  readSequentialData(jsonFile: SequentialDataJson): SequentialData {
    const channelNames: string[] = [];
    const data: number[][] = [jsonFile.time];
    for (const [key, value] of Object.entries(jsonFile)) {
      if (key !== "time") {
        channelNames.push(key);
        data.push(value)
      }
    }
    return { "channelNames": channelNames, "data": data };
  }

  readLabels(): void {
    this._labels = labelsFile.map(lab => [lab.time_start, lab.time_end]);
  }

  readPredictionsWithConfidence(): void {
    this._predictionsWithConfidence = predictionsWithConfidenceFile.map(pred => [pred.time_start, pred.time_end, pred.confidence]);
  }

  getPredWithConfAboveThres(): number[][] {
    return this._predictionsWithConfidence.filter(pred => pred[2] >= this._threshold)
  }

  // Setters and getters
  set query(v: number[][]) {
    this._query = v;
  }
  get query(): number[][] {
    return this._query;
  }

  set query_reconstructed(v: number[][]) {
    this._queryReconstructed = v;
  }
  get query_reconstructed(): number[][] {
    return this._queryReconstructed;
  }

  set timeSeries(v: number[][]) {
    this._timeSeries = v;
  }
  get timeSeries(): number[][] {
    return this._timeSeries;
  }

  set timeSeriesReconstructed(v: number[][]) {
    this._timeSeriesReconstructed = v;
  }
  get timeSeriesReconstructed(): number[][] {
    return this._timeSeriesReconstructed;
  }

  set labels(v: number[][]) {
    this._labels = v;
  }
  get labels(): number[][] {
    return this._labels;
  }

  set predictionsWithConfidence(v: number[][]) {
    this._predictionsWithConfidence = v;
  }
  get predictionsWithConfidence(): number[][] {
    return this._predictionsWithConfidence;
  }

  set threshold(v: number) {
    this._threshold = v;
  }
  get threshold(): number {
    return this._threshold;
  }

  set confidence(v: number[][]) {
    this._confidence = v;
  }
  get confidence(): number[][] {
    return this._confidence;
  }

  set mainViewXAxisRange(newRange: number[]) {
    this._mainViewXAxisRange = newRange;
    // this._mainViewXAxisRange.next(newRange);
  }
  get mainViewXAxisRange() {
    return this._mainViewXAxisRange;
  }
}
