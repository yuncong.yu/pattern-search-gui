import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { StateService } from '../state.service';

declare var Plotly: any;

@Component({
  selector: 'app-confidence-view',
  templateUrl: './confidence-view.component.html',
  styleUrls: ['./confidence-view.component.css']
})
export class ConfidenceViewComponent implements OnInit {

  data = [];
  layout: {};
  config: {};
  localXAxisRange: number[] = null;

  @ViewChild("confidenceGraph")
  graph: ElementRef;

  constructor(private state: StateService) {
    this.prepareData();
    this.prepareLayoutConfig();
  }

  ngOnInit(): void {
    this.graph = Plotly.react('confidenceView', this.data, this.layout, this.config);
  }

  ngAfterViewInit() {
    // Layout related interactions: zoom, threshold tuning
    this.graph.nativeElement.on(
      "plotly_relayout",
      // Unfortunately, this handler can't be put in a seperate function, 
      // otherwise, "this" would be the event itself but not the component class
      (data: {}) => {
        // For debug
        // console.log(data);

        // Number of channels
        let n_channels = this.state.timeSeries.length - 1;
        
        // (Oberservable based solution)
        // Bind x-axis of the confidence view with the main view
        // Avoid recursive resizing
        // if ("xaxis.range" in data) return
        // // if ("xaxis.autorange" in data && this.localXAxisRange !== null) {
        // if ("xaxis.autorange" in data) {
        //   this.localXAxisRange = null;
        //   this.state.mainViewXAxisRange = this.localXAxisRange;
        // }
        // if ("xaxis.range[0]" in data) {
        //   this.localXAxisRange = [data['xaxis.range[0]'], data['xaxis.range[1]']];
        //   this.state.mainViewXAxisRange = this.localXAxisRange;
        // }
        if ("xaxis.autorange" in data && this.state.mainViewXAxisRange !== null) {
          this.state.mainViewXAxisRange = null;
          Plotly.relayout("mainHeatmap", { 'xaxis.autorange': true });
          Plotly.relayout("mainLinePlot", { 'xaxis.autorange': true });
        }
        if ("xaxis.range[0]" in data) {
          this.state.mainViewXAxisRange = [data['xaxis.range[0]'], data['xaxis.range[1]']];
          Plotly.relayout("mainHeatmap", { 'xaxis.range': this.state.mainViewXAxisRange })
          Plotly.relayout("mainLinePlot", { 'xaxis.range': this.state.mainViewXAxisRange })
        }

        // Handle threshold tuning
        if ("shapes[0].x0" in data && data["shapes[0].y0"] != this.state.threshold) {
          this.state.threshold = data["shapes[0].y0"];
          let paras = this.calculatePredictionsTraceParameters()

          let dataConfidenceViewUpdate = {
            "x": [paras.positions, paras.positions],
            "y": [
              Array(paras.positions.length).fill(paras.yMin),
              Array(paras.positions.length).fill(paras.yRange)
            ],
            "width": [paras.width, paras.width],
            "text": [[], paras.text]
          }

          let dataMainViewUpdate = {
            "x": [paras.positions, paras.positions],
            "y": [],
            "width": [paras.width, paras.width],
            "text": [[], paras.text]
          }
          this.state.timeSeries.slice(1).forEach(
            value => {
              let yMin = Math.min(...value);
              let yRange = Math.max(...value) - yMin;
              dataMainViewUpdate["y"].push(Array(paras.positions.length).fill(yMin));
              dataMainViewUpdate["y"].push(Array(paras.positions.length).fill(yRange));
            }
          )

          let layoutUpdate = {
            "shapes[0].x0": 0,
            "shapes[0].x1": 1,
            "shapes[0].y0": this.state.threshold,
            "shapes[0].y1": this.state.threshold,
            "annotations[0].text": this.state.threshold.toFixed(2),
            "annotations[0].y": this.state.threshold
          }

          const tracesConfidenceViewUpdate = [1, 2];
          const tracesMainViewUpdate = [];
          for (let i = 4 * n_channels; i < 6 * n_channels; i++) {
            tracesMainViewUpdate.push(i)
          }

          Plotly.update(
            "confidenceView",
            dataConfidenceViewUpdate,
            layoutUpdate,
            tracesConfidenceViewUpdate
          )

          Plotly.update(
            "mainLinePlot",
            dataMainViewUpdate,
            [],
            tracesMainViewUpdate,
          )
        }
      }
    )

    // Legend click related interactions
    this.graph.nativeElement.on(
      "plotly_legendclick",
      // Unfortunately, this handler can't be put in a seperate function, 
      // otherwise, "this" would be the event itself but not the component class
      (data) => {
        // For debug
        // console.log(data);
        if (data.curveNumber === 3) {
          let newStatus = (data.fullData[3].visible === true) ? false : true;
          Plotly.relayout("confidenceView", { 
            "shapes[0].visible": newStatus,
            "annotations[0].visible": newStatus
          })
        }
      }
    )

    // Subscribe to x-range change (Oberservable based solution)
    // this.state.mainViewXAxisRangeObservable.subscribe(this.updateLocalXAxis)
  }

  prepareData(): void {
    // Confidence
    this.data.push({
      x: this.state.confidence[0],
      y: this.state.confidence[1],
      mode: "lines",
      xaxis: "x",
      yaxis: "y",
      line: { color: '#1f77b4' },  // muted blue (tableau blue)
      name: "confidence",
      showlegend: true,
    });

    let paras = this.calculatePredictionsTraceParameters();
    const bottomTrace = {
      x: paras.positions,
      y: Array(paras.positions.length).fill(paras.yMin),
      xaxis: "x",
      yaxis: "y",
      width: paras.width,
      type: "bar",
      marker: {
        color: "rgba(1, 1, 1, 0.0)",
        line: { width: 0 }
      },
      legendgroup: "predictions with confidence",
      showlegend: false
    };

    const topTrace = {
      x: paras.positions,
      y: Array(paras.positions.length).fill(paras.yRange),
      xaxis: "x",
      yaxis: "y",
      width: paras.width,
      text: paras.text,
      type: "bar",
      marker: {
        color: '#2ca02c',  // cooked asparagus green (tableau green)
        line: { width: 0 }
      },
      opacity: 0.4,
      name: "predictions with confidence",
      textposition: 'auto',
      hoverinfo: 'none',
      legendgroup: "predictions",
      showlegend: true
    };

    this.data.push(bottomTrace);
    this.data.push(topTrace);

    // Threshold as a trace
    // console.log([this.state.confidence[0][0], this.state.confidence[0][1]])
    this.data.push({
      // visible: "legendonly",
      x: [null],
      y: [null],
      // x: [this.state.confidence[0][0], this.state.confidence[0][this.state.confidence[0].length - 1]],
      // y: [this.state.threshold, this.state.threshold],
      text: ["", this.state.threshold.toFixed(2)],
      textposition: "top right",
      textfont: { color: "#d62728" },  // brick red (Tableau red)
      mode: "lines+text",
      // mode: "lines",
      xaxis: "x",
      yaxis: "y",
      line: { 
        color: "#d62728",  // brick red (Tableau red)
        dash: "dot"
      },
      name: "threshold"
    })
  }

  prepareLayoutConfig(): void {
    // Initialize layout
    this.layout = {
      // Threshold line
      barmode: "stack",
      shapes: [{
        type: 'line',
        xref: 'x domain',
        x0: 0,
        y0: this.state.threshold,
        x1: 1,
        y1: this.state.threshold,
        line: {
          color: "#d62728",  // brick red (Tableau red)
          dash: 'dot'
        },
        editable: true,
      }],
      // Threshold value as annotation
      annotations: [
        {
          xref: "paper",
          yref: "paper",
          x: 1,
          y: this.state.threshold,
          xanchor: "right",
          yanchor: "bottom",
          text: this.state.threshold.toFixed(2),
          font: {
            color: "#d62728",  // brick red (Tableau red),
          },
          showarrow: false,
          name: "threshold",
        }
      ],
      // grid: { rows: 1, columns: 1, subplots: [["xy"]] },
      height: 200,
      legend: {
        "orientation": "h",
        x: 0,
        y: -0.2
      },
      margin: {
        l: 30,
        r: 30,
        t: 30,
        b: 30,
        pad: 4
      },
    };

    // Initialize config
    this.config = {
      responsive: true,
      displaylogo: false,
      edits: {
        shapePosition: true,
        // annotationPosition: true
      }
    };
  }

  calculatePredictionsTraceParameters(): { positions: number[], width: number[], text: string[], yMin: number, yRange: number } {
    let chosenPreds = this.state.getPredWithConfAboveThres();
    let positions = chosenPreds.map(pred => (pred[0] + pred[1]) / 2);
    let width = chosenPreds.map(pred => (pred[1] - pred[0]));
    let text = chosenPreds.map(pred => pred[2].toFixed(2))

    let yMin = Math.min(...this.state.confidence[1]);
    let yMax = Math.max(...this.state.confidence[1]);
    let yRange = yMax - yMin;

    return {
      positions: positions,
      width: width,
      text: text,
      yMin: yMin,
      yRange: yRange
    }
  }

  // (Oberservable based solution)
  // updateLocalXAxis(newRange: number[]) {
  //   if (newRange == null && this.localXAxisRange !== null) {
  //     this.localXAxisRange = null;
  //     Plotly.relayout("confidenceView", { 'xaxis.autorange': true })
  //   }
  //   // else if (JSON.stringify(newRange) != JSON.stringify(this.localXAxisRange)) {
  //   else {
  //     this.localXAxisRange = newRange;
  //     // console.log(this.localXAxisRange)
  //     Plotly.relayout("confidenceView", { 'xaxis.range': newRange })
  //   }
  // }
}
