import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfidenceViewComponent } from './confidence-view.component';

describe('ConfidenceViewComponent', () => {
  let component: ConfidenceViewComponent;
  let fixture: ComponentFixture<ConfidenceViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfidenceViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfidenceViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
